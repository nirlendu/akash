/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'

import TopMessage from  'app/containers/index/TopMessage'
import FirstDescription from  'app/containers/index/FirstDescription'
import Partners from 'app/containers/index/Partners'
import About from 'app/containers/footer/About'

import Constants from'app/config/core/constants'
import CoreStyle from  'app/config/core/style'

export default class Index extends React.Component {11

	render() {
		return (
			<div>
				<TopMessage />
				<FirstDescription />
				<Partners />
				<About />
			</div>
		)
	}
}
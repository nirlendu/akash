/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

import PrivacyBody from'app/containers/policy/privacy/Privacy'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			position: 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			width: '1000px',
			paddingBottom: '5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			position: 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			width: '100%',
			paddingBottom: '15%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			position: 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			width: '100%',
			paddingBottom: '15%',
		},
	},
})

export default class Privacy extends React.Component {
	render() {
		return (
			<div style={Style.Wrapper}>
				<PrivacyBody/>
			</div>
		)
	}
}

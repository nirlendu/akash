/**
 * Copyright - Panally Internet
 */

var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');
var match = ReactRouter.match;

var RouterContext = React.createFactory(ReactRouter.RouterContext);
var Provider = React.createFactory(require('react-redux').Provider);

var routes = require('app/config/routes').routes;

var mountNode = document.getElementById('react-mount');

match({routes: routes, location: '/policy/terms'}, function(error, redirectLocation, renderProps) {
	if (error) {
		 // TODO
	} 
	ReactDOM.render(
		RouterContext(renderProps),
		mountNode
	);
}); 
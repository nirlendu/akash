/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import Helmet from 'react-helmet'

class Include extends React.Component {
	render() {
		return (
			<Helmet
				title="Policy"
			/>
		);
	}
}


module.exports = Include;

/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

import Include from 'app/pages/policy/Include'
import PolicyNavbar from 'app/containers/policy/navbar/MainNavbar'


const Style = StyleSheet.create({
	PageContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '85%',
			margin: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '85%',
			margin: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '90%',
			margin: 'auto',
		},
	},
})


class Policy extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			showChildren: false,
			prebootHTML: this.props.params.prebootHTML
		};
	}

	componentDidMount() {
		this.setState({ prebootHTML: '', showChildren: true })
	};

	render() {
		return (
			<div>
				<Include/>
				<PolicyNavbar/>
				<div className={css(Style.PageContainer)}>
					{this.props.children}
				</div>
			</div>
		)
	}
}

module.exports = Policy;

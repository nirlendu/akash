/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CampaignBody from 'app/containers/campaign/CampaignBody'

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '2%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '2%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '10%'
		},
	}
});

export default class Business extends React.Component {
	render() {
		return (
			<div className={css(Style.Container)}>
				<CampaignBody
					data={this.props.data}
				/>
			</div>
		)
	}
}

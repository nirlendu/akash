/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { connect }  from 'react-redux'

import Campaign from 'app/pages/campaign/Campaign'
import Include from 'app/pages/campaign/Include'

class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<Campaign
					data={this.props.data}
				/>
			</div>
		);
	}
}

const PageState = function(state){
	return {
		data: state.data
	}
};

Page = connect(
	PageState
)(Page);

module.exports = Page;

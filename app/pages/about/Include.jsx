/**
 * Copyright - Panally Internet
 */

import React from 'react'
import Helmet from 'react-helmet'

class Include extends React.Component {
	render() {
		return (
			<Helmet
				title="About Us"
				meta={[
					{name: "description", content: "About Panally : What we do and want to do"},
				]}
			/>
		);
	}
}

module.exports = Include;

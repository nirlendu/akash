/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

import Include from 'app/pages/about/Include'

import Header from 'app/containers/about/Header'
import AboutBody from 'app/containers/about/AboutBody'
import TeamBody from 'app/containers/about/TeamBody'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '85%',
			margin: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			paddingBottom: '15%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			paddingBottom: '15%',
		},
	},
})

export default class Page extends React.Component {
	render() {
		return (
			<div id="react-mount">
				<Include/>
				<div className={css(Style.Wrapper)}>
					<Header
						data='What is it all about?'
					/>
					<AboutBody/>
					<Header
						data='People @ Panally'
					/>
					<TeamBody/>
				</div>
			</div>
		)
	}
}

/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react';
import { StyleSheet, css } from 'aphrodite'
import PropTypes from 'prop-types'

import Url from 'app/config/core/url'
import CoreStyle from 'app/config/core/style'

const Style = StyleSheet.create({
	FlashStyle:{
		fill: '#ffbd4a',
	},
})

class InstantIcon extends React.Component {

	render() {
		return (
			<svg
				width={this.props.width} 
				height={this.props.height} 
				viewBox={this.props.viewBox}
				className={css(Style.FlashStyle)}
				xmlns="http://www.w3.org/2000/svg"
			>
				<path d="M11.001,30l2.707-16.334H5L11.458,0l9.25,0.123L16.667,8H25L11.001,30z"/>
			</svg>
		);
	}
}

InstantIcon.propTypes = {
	width: PropTypes.string.isRequired,
	height: PropTypes.string.isRequired,
	viewBox: PropTypes.string.isRequired,
}

module.exports = InstantIcon;

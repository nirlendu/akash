/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import { ShareButtons, generateShareIcon } from 'react-share'

const { 
	FacebookShareButton, 
	WhatsappShareButton, 
	EmailShareButton 
} = ShareButtons;

const FacebookIcon = generateShareIcon('facebook')
const WhatsappIcon = generateShareIcon('whatsapp')
const EmailIcon = generateShareIcon('email')

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Title: {
		[CoreStyle.PC.BREAKPOINT]:{
			textAlign: 'center',
			paddingBottom: '1%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREEN]
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			textAlign: 'center',
			paddingBottom: '1%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREEN]
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			textAlign: 'center',
			paddingBottom: '1%',
			fontSize: '20px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREEN]
		},
	},
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '2% 43%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '2%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '5% 20%'
		},
	},
});

export default class Share extends React.Component {
	render() {
		const title = 'Visit ' + this.props.businessName[0] + ' & claim your referral to get ' + this.props.campaignDetails;
		const shareUrl = 'https://panally.com/r/' + this.props.campaignId;
		const pictureUrl = '';
		return (
			<div>
				<div className={css(Style.Title)}>
					Share Now!
				</div>
				<div className={css(Style.Container)}>
					<FacebookShareButton
						url={shareUrl}
						quote={title}
						picture={pictureUrl}
					>
						<FacebookIcon
							size={50}
							round 
						/>
					</FacebookShareButton>
					&nbsp;
					&nbsp;
					&nbsp;
					<WhatsappShareButton
						className={css(Style.EachButton)}
						url={shareUrl}
						title={title}
						separator=" : "
					>
						<WhatsappIcon 
							size={50} 
							round 
						/>
					</WhatsappShareButton>
					&nbsp;
					&nbsp;
					&nbsp;
					<EmailShareButton
						url={shareUrl}
						subject="Referral!"
						body={title}
					>
						<EmailIcon
							size={50}
							round 
						/>
					</EmailShareButton>
				</div>
			</div>
		)
	}
}
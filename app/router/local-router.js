/**
 * Copyright - Panally Internet
 */


/******************
 *
 *
 * Flow of the router config, () signifies next in the heirarchy
 * 
 * local-router --> (static-router) --> (place-rouer) --> (payment-router) --> (main-router)
 *
 *
 *******************/


// Imports
const express = require('express');
const fs = require('fs');    
const path = require('path');
const url = require('url');
const includes = require('lodash/includes');

// Initializing the router
const router = express.Router();

router.get('/api/index', function(req, res) { 
	
	const IndexFile = path.join('schema/index.json');  
	fs.readFile(IndexFile, function(err, data) {
		if (err) {
			console.error(err);
			res.json({});
			res.end();
		}else{
			res.json(JSON.parse(data));
		}
	});
});

router.get('/api/listing/:listingType', function(req, res) { 

	let File = null;
	if(req.params.listingType == 'places'){
		File = path.join('schema/listing/places.json'); 
	}
	if(req.params.listingType == 'journeys'){
		File = path.join('schema/listing/journeys.json'); 
	}
	if(req.params.listingType == 'activities'){
		File = path.join('schema/listing/activities.json'); 
	} 
	if(req.params.listingType == 'excursions'){
		File = path.join('schema/listing/excursions.json'); 
	} 
	if(req.params.listingType == 'events'){
		File = path.join('schema/listing/events.json'); 
	} 
	fs.readFile(File, function(err, data) {
		if (err) {
			console.error(err);
			res.json({});
			res.end();
		}else{
			res.json(JSON.parse(data));
		}
	});
});

router.get('/api/nav-search', function(req, res) {

	const SearchFile = path.join('schema/search.json');
	fs.readFile(SearchFile, function(err, data) {
		if (err) {
			console.error(err);
			res.json({});
			res.end();
		}else{
			res.json(JSON.parse(data));
		}
	});
});

router.get('/api/business/:code', function(req, res) {
	
	const JourneyFile = path.join('schema/business.json');
	fs.readFile(JourneyFile, function(err, data) {
		if (err) {
			console.error(err);
			res.json({});
			res.end();
		}else{
			res.json(JSON.parse(data));
		}
	});
});

router.get('/api/campaign/:code', function(req, res) {
	
	const CampaignFile = path.join('schema/campaign.json');
	fs.readFile(CampaignFile, function(err, data) {
		if (err) {
			console.error(err);
			res.json({});
			res.end();
		}else{
			res.json(JSON.parse(data));
		}
	});
});

module.exports = router;

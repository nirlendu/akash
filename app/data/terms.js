/**
 * Copyright - Panally Internet
 */

var Enum = require('enum');

var Terms = {
	"webTerms": [
		"Right to admission is reserved with the respective business.",
		"Operational aspects of the campaign including validity is controlled by the business."
	]
}

module.exports = new Enum(Terms).toJSON();
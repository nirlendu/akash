/**
 * Copyright - Panally Internet
 */

import React from 'react'

import {Route, Router, IndexRoute, browserHistory} from 'react-router'

import App from 'app/pages/App'
import Index from 'app/pages/index/Page'

import Campaign from 'app/pages/campaign/Page'

import Policy from 'app/pages/policy/Policy'
import Privacy from 'app/pages/policy/privacy/Page'
import Terms from 'app/pages/policy/terms/Page'

import FourOhFour from 'app/pages/error/404/Page'

import About from 'app/pages/about/Page'
import Contact from 'app/pages/contact/Page'

if (typeof window === 'object') {
    function createElement(Component, props) {
        return <Component {...props} {...window.PROPS} />;
    }
}

const routes = (
	<Router history={browserHistory}>
		<Route path='/' component={App}>
			<IndexRoute component={Index}/>
			<Route path='r/:code' component={Campaign}/>
			<Route path='policy/'component={Policy}>
				<Route path='privacy' component={Privacy}/>
				<Route path='terms' component={Terms}/>
			</Route>
			<Route path='oops/'>
				<Route path='404' component={FourOhFour}/>
			</Route>
			<Route path='about' component={About}/>
			<Route path='contact' component={Contact}/>
		</Route>
    </Router>
)

module.exports = {
  routes: routes
}

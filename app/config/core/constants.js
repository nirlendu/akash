/**
 * Copyright - Panally Internet
 */

var Enum = require('enum');

var Constants = {
	'Type' : {
		'Place' : 'PLACE',
		'Journey' : 'JOURNEY',
		'Activity' : 'ACTIVITY',
		'Excursion' : 'EXCURSION',
		'Event' : 'EVENT',
		'Listing': {
			'Featured': 'FEATURED',
			'Mixed': 'MIXED'
		},
	},
	'Plural' : {
		'Place' : 'PLACES',
		'Journey' : 'JOURNEYS',
		'Activity' : 'ACTIVITIES',
		'Excursion' : 'EXCURSIONS',
		'Event' : 'EVENTS'
	},
	'Place' : {
		'ThingsToDo' : 'THINGS-TO-DO',
		'PlacesToEat' : 'PLACES-TO-EAT',
		'PlacesToSee' : 'PLACES-TO-SEE',
		'NearbyPlaces' : 'NEARBY-PLACES',
		'HelpfulTips' : 'HELPFUL-TIPS'
	}
}

module.exports = new Enum(Constants).toJSON();
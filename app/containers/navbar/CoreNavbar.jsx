/**
 * Copyright - Panally Internet
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import url from 'app/config/core/url'
import CoreStyle from 'app/config/core/style'
import PanallyWhite from 'app/icons/PanallyWhite'

import SearchNav from 'app/containers/navbar/SearchNav'

const Style = StyleSheet.create({
	Parent: {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			borderRadius: '0px',
			marginBottom: '0px',
			zIndex: '10',
			fontSize: '20px',
			paddingTop: '0.5%',
			backgroundColor: 'white',
			border: 'none',
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			borderRadius: '0px',
			zIndex: '10',
			fontSize: '20px',
			backgroundColor: [CoreStyle.COLOR.WHITE],
			border: 'none',
			fontWeight: 'bold',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			borderRadius: '0px',
			marginBottom: '0px',
			zIndex: '10',
			fontSize: '20px',
			paddingTop: '1%',
			paddingBottom: '1%',
			backgroundColor: [CoreStyle.COLOR.WHITE],
			border: 'none',
			fontWeight: 'bold',
		},
	},
	Navbar : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'block',
			backgroundColor: 'inherit',
			width: '20%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			overflowY: 'hidden',
			height: '50px',
			backgroundColor: 'inherit',
			width: '30%',
			marginLeft: 'auto',
			marginRight: 'auto',
			position: 'relative',
			overflowX: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			overflowY: 'hidden',
			height: '50px',
			backgroundColor: 'inherit',
			width: '40%',
			position: 'relative',
			overflowX: 'auto',
		},
	},
	SearchBar: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '30%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '30%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '40%',
			marginLeft: '1%',
		},
	},
	LogoBase : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: 'auto',
			cursor: 'pointer',
			marginLeft: '3%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: 'auto',
			cursor: 'pointer',
			marginLeft: '2%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: 'auto',
			cursor: 'pointer',
			marginLeft: '2%',
		},
	},
	BetaText : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			color: [CoreStyle.COLOR.BLACK],
			fontSize: '15px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			color: [CoreStyle.COLOR.BLACK],
			fontSize: '15px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			color: [CoreStyle.COLOR.BLACK],
			fontSize: '10px',
		},
	},
	Logo : {
		[CoreStyle.PC.BREAKPOINT]:{
			height: '50px',
			width: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			height: '50px',
			width: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			height: '50px',
			width: 'auto',
		},
	},
	MenuBarContainer:{
		[CoreStyle.PC.BREAKPOINT]:{
			width: '50%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '50%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '0%',
		},
	},
	MenuBar:{
		[CoreStyle.PC.BREAKPOINT]:{
			paddingLeft: '15%',
			paddingTop: '2%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
		},
		[CoreStyle.MOB.BREAKPOINT]:{
		},
	},
	Container:{
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			margin: '0px',
			listStyle: 'none',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
		},
	},
	Button:{  
		[CoreStyle.PC.BREAKPOINT]:{
			paddingLeft:'65%', 
		}, 
		[CoreStyle.TAB.BREAKPOINT]:{
			padding:'0% 25px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding:'3%', 
		},
	},
	Link:{
		color: [CoreStyle.COLOR.GREY],
		':hover':{
			color: [CoreStyle.COLOR.RED],
			backgroundColor: 'inherit',
		},
	},
	CollapseContainer:{
		[CoreStyle.PC.BREAKPOINT]:{
			border: 'none',
			width: '50%',
			marginLeft: '50%',
			padding: '0px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			border: 'none',
			width: '50%',
			margin: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			border: 'none',
			width: '100%',
			padding: '0px',
			textAlign: 'right',
			backgroundColor: 'white',
		},
	},
	ExpandButton:{
		padding: '1% 3%',
		backgroundColor: 'rgba(0,0,0,0.07)',
		color: [CoreStyle.COLOR.GREY],
	}
})

export default class CoreNavbar extends React.Component {
	
	render() {
		return (
			<header>
				<div className="inner">
					<nav className={css(Style.Parent)}>
						<div className={css(Style.Navbar)}>
							<a href='/' className={css(Style.LogoBase)}>
								<PanallyWhite
									width="120"
									height="50"
									viewBox="50 0 20 100"
									imageWidth="550"
									imageHeight="100"
									imageX="-220"
									imageY="0"
								/>
							</a>
							<span className={css(Style.BetaText)}>&nbsp; beta</span>
						</div>
						<div className={css(Style.SearchBar)}>
							<SearchNav/>
						</div>
						<div className={css(Style.MenuBarContainer)}>
							<div className={css(Style.MenuBar)}>
								<input className="checkbox-navbar" type="checkbox" id="nav" /><label className="label-navbar" htmlFor="nav"></label>
								<ul className={css(Style.Container)}>
									<li key={4} className={css(Style.Button)}>
										<a key={4} className={css(Style.Link)} href='/contact' target="_blank" rel="noopener noreferer">Contact</a>
									</li>
								</ul>
							</div>
						</div>
					</nav>
				</div>
			</header> 
		);
	}
}

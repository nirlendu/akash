/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import _ from 'lodash/includes'
import request from 'superagent'

import AutoComplete from 'material-ui/AutoComplete';

import Constants from'app/config/core/constants'
import CoreStyle from  'app/config/core/style'
import Url from 'app/config/core/url'
import SearchIcon from 'app/icons/SearchIcon'

const Style = StyleSheet.create({
	Container:{
		[CoreStyle.PC.BREAKPOINT]: {
			display:'flex',
			maxWidth:'100%',
			marginTop: '0.5%',
			paddingLeft: '2%',
			backgroundColor: '#EFEFEF',
		},
		[CoreStyle.TAB.BREAKPOINT]: {
			display:'flex',
			maxWidth:'100%',
			whiteSpace:'nowrap',
			overflow:'hidden',
			marginTop: '0.5%',
			paddingLeft: '2%',
			backgroundColor: '#EFEFEF',
		},
		[CoreStyle.MOB.BREAKPOINT]: {
			display:'flex',
			maxWidth:'100%',
			whiteSpace:'nowrap',
			overflow:'hidden',
			marginTop: '1%',
			paddingLeft: '5%',
			backgroundColor: '#EFEFEF',
		},
	}
})

export default class SearchNav extends React.Component {

	constructor() {
		super();
		this.onNewRequest = this.onNewRequest.bind(this);
		this.filter = this.filter.bind(this);
		this.state = {
			dataSource: [],
			code: {}
		};
	}

	componentDidMount(){

		const requestUrl = '/api/nav-search';
		
		request
		.get(requestUrl)
		.end(function(error, response){
			if (error) {
				//TODO
			}
			const data = response.body;
			let dataSource = [];
			let parentCode = {};
			data.journeys.map(function(eachJourney){
				dataSource.push(eachJourney.name + ' - in Journeys');
				const dict = {};
				dict['code'] = eachJourney.code;
				dict['slug'] = eachJourney.slug;
				parentCode[eachJourney.name] = dict;
			});
			data.activities.map(function(eachActivity){
				dataSource.push(eachActivity.name + ' - in Activities');
				const dict = {};
				dict['code'] = eachActivity.code;
				dict['slug'] = eachActivity.slug;
				parentCode[eachActivity.name] = dict;
			});
			data.excursions.map(function(eachExcursion){
				dataSource.push(eachExcursion.name + ' - in Excursions');
				const dict = {};
				dict['code'] = eachExcursion.code;
				dict['slug'] = eachExcursion.slug;
				parentCode[eachExcursion.name] = dict;
			});
			data.events.map(function(eachEvent){
				dataSource.push(eachEvent.name + ' - in Events');
				const dict = {};
				dict['code'] = eachEvent.code;
				dict['slug'] = eachEvent.slug;
				parentCode[eachEvent.name] = dict;
			});
			data.places.map(function(eachPlace){
				dataSource.push(eachPlace.name + ' - in Places');
				const dict = {};
				dict['code'] = eachPlace.code;
				dict['slug'] = null;
				parentCode[eachPlace.name] = dict;
			});
			this.setState({
				dataSource: dataSource,
				code: parentCode
			});
		}.bind(this));
	}

	onNewRequest(value){
		if (_(this.state.dataSource, value)){
			let type = null;
			if(_(value.toUpperCase(), Constants.Plural.Journey)){
				type = Constants.Type.Journey;
			}
			if(_(value.toUpperCase(), Constants.Plural.Activity)){
				type = Constants.Type.Activity;
			}
			if(_(value.toUpperCase(), Constants.Plural.Excursion)){
				type = Constants.Type.Excursion;
			}
			if(_(value.toUpperCase(), Constants.Plural.Event)){
				type = Constants.Type.Event;
			}
			if(_(value.toUpperCase(), Constants.Plural.Place)){
				type = Constants.Type.Place;
			}
			const queryUrl = '/nav-search?type=' + type + '&code=' + this.state.code[value.split(' - in ')[0]].code + '&slug=' + this.state.code[value.split(' - in ')[0]].slug;
			window.location.replace(queryUrl);
		}
	}

	filter(searchText, key) {
		return key.toLowerCase().includes(searchText.toLowerCase());
	}

	render() {
		return (
			<div className={css(Style.Container)}>
				<span>
					<SearchIcon
						width="20"
						height="40"
						viewBox="20 -50 300 300"
					/>
					&nbsp;&nbsp;
				</span>
				<AutoComplete
					hintText="Where?"
					menuStyle={{width: '100%', fontWeight: 'normal'}}
 					listStyle={{width: '100%'}}
					underlineStyle={{display: 'none'}}
					dataSource={this.state.dataSource}
					onNewRequest={this.onNewRequest}
					filter={this.filter}
				/>
			</div>
		);
	}
}

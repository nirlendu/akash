/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import ProgressiveImage from 'react-progressive-image'

import CoreStyle from 'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			paddingTop: '4%',
			marginLeft: '7.5%',
			marginRight: 'auto',
			width: '85%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			marginBottom: '0.5%',
			marginTop: '5px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			marginBottom: '0.5%',
			marginTop: '5px',
		},
	},
	ImageParent : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			verticalAlign: 'top',
			height: 'auto',
			width: '50%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			verticalAlign: 'top',
			height: 'auto',
			width: '100%',
			margin: '25px 0',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			verticalAlign: 'top',
			height: 'auto',
			width: '100%',
			margin: '25px 0',
		}
	},
	Image : {
		[CoreStyle.PC.BREAKPOINT]:{
			height: '100%',
			width: '100%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			height: '100%',
			width: '100%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			height: '100%',
			width: '100%'
		}
	},
	FlexContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			verticalAlign: 'top',
			height: 'auto',
			width: '50%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			verticalAlign: 'top',
			height: 'auto',
			width: '100%',
			marginTop: [CoreStyle.TAB.SUBSECTION_MARGIN],
			marginBottom: [CoreStyle.TAB.SUBSECTION_MARGIN],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			verticalAlign: 'top',
			height: 'auto',
			width: '100%',
			marginTop: [CoreStyle.MOB.SUBSECTION_MARGIN],
			marginBottom: [CoreStyle.MOB.SUBSECTION_MARGIN],
		}
	},
	BlockContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			paddingRight: '15%',
			paddingLeft: '5%',
			paddingTop: '15%',
			textAlign: 'right',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			padding: '0px 5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			padding: '0px 5%',
		}
	},
	Title : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'block',
			fontWeight: 'bold',
			fontSize: '30px',
			color : [CoreStyle.COLOR.GREEN],
			width: '100%',
			paddingLeft: '2%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			fontWeight: 'bold',
			fontSize: [CoreStyle.TAB.TITLE_FONTSIZE],
			color : [CoreStyle.COLOR.GREEN],
			textAlign: 'center',
			width: '100%',
			marginTop: '5%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			fontWeight: 'bold',
			fontSize: [CoreStyle.MOB.TITLE_FONTSIZE],
			color : [CoreStyle.COLOR.GREEN],
			textAlign: 'center',
			width: '100%',
			marginTop: '5%'
		}
	},
	Description : {
		[CoreStyle.PC.BREAKPOINT]:{
			marginTop: '5%',
			display: 'block',
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			color : [CoreStyle.COLOR.GREY],
			width: '100%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginTop: '3%',
			textAlign: 'center',
			display: 'block',
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			color : [CoreStyle.COLOR.GREY],
			width: '100%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginTop: '3%',
			textAlign: 'center',
			display: 'block',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			color : [CoreStyle.COLOR.GREY],
			width: '100%'
		}
	}
})

class FirstDescription extends React.Component {

	render() {
		const PlaceholderImage = Url.Static.App.PlaceholderImage540x350;
		const ImageFile = Url.Static.App.Endpoint + 'index/sale.jpg';
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.ImageParent)}>
					<ProgressiveImage 
						className={css(Style.Image)} 
						src={ImageFile} placeholder={PlaceholderImage} alt='Panally Places'
					>
						{(src) => <img className={css(Style.Image)} src={src} alt='Panally Places'/>}
					</ProgressiveImage>
				</div>
				<div className={css(Style.FlexContainer)}>
					<div className={css(Style.BlockContainer)}>
						<div className={css(Style.Title)}>
							Increase your audience & sales
						</div>
						<div className={css(Style.Description)}>
							By spending less on the right way of marketing, target potential customers which in turn will help in increasing your sales with a minimal marketing budget.
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default FirstDescription;

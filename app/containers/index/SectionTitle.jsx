/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from 'app/config/core/style'

const Style = StyleSheet.create({
	Parent:{
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			textAlign: 'left',
			marginLeft: '7.5%',
			marginRight: 'auto',
			width: '85%',
			paddingTop: '2%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			textAlign: 'left',
			paddingBottom: '25px',
			width: '100%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			textAlign: 'left',
			paddingTop: '7%',
			paddingLeft: '2.5%',
			width: '100%',
		},
	},
	Title : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '92%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '80%',
			fontSize: [CoreStyle.TAB.TITLE_FONTSIZE],
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '60%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.GREY],
			letterSpacing: '-1px',
		},
	},
	Description : {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '0.5%',
			fontSize: '20px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingLeft: '15%',
			paddingTop: '0.5%',
			fontSize: '17px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingLeft: '20%',
			paddingTop: '0.5%',
			fontSize: '20px',
		},
	},
	SeeAllLink:{
		color: [CoreStyle.COLOR.YELLOW],
		':hover':{
			color: [CoreStyle.COLOR.BLUE],
			cursor:'pointer',
		}
	}

})

export default class SectionTitle extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.Title)}>
					{this.props.title}
				</div>
				<div className={css(Style.Description)}>
					{
						this.props.url
						?
						<a className={css(Style.SeeAllLink)} href={this.props.url}>
							More >
						</a>
						:
						<div/>
					}
				</div>
			</div>
		);
	}
}

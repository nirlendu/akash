/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import Constants from'app/config/core/constants'
import CoreStyle from 'app/config/core/style'

import Activity from 'app/common/containers/listing/Activity'
import Journey from 'app/common/containers/listing/Journey'
import Excursion from 'app/common/containers/listing/Excursion'
import Event from 'app/common/containers/listing/Event'
import Place from 'app/common/containers/listing/Place'

const Style = StyleSheet.create({
	Parent: {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '1.5%',
			marginLeft: '7.5%',
			marginRight: 'auto',
			width: '85%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
		},
	},
	ParentQuarterContainer: {
		[CoreStyle.PC.BREAKPOINT]:{
			marginLeft: '-5px',
			marginRight: '-5px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginLeft: '-8px',
			marginRight: '-8px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginLeft: '-12px',
			marginRight: '-12px',
		},
	},
	ParentSubQuarterContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
			display: 'inline-flex',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			whiteSpace: 'nowrap',
			overflowX: 'auto',
			overflowY: 'hidden',
			transition: '-ms-transform 0.5s, -webkit-transform 0.5s, transform 0.5s',
			padding: '0px 18px',
		}
	},
	QuarterContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '25%',
			height: 'auto',
			display: 'inline-block',
			padding: '0.5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '45%',
			height: 'auto',
			display: 'inline-block',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '48%',
			height: 'auto',
			display: 'inline-block',
		},
	},
	ThirdContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '33%',
			height: 'auto',
			display: 'inline-block',
			padding: '0.5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '33%',
			height: 'auto',
			display: 'inline-block',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '66%',
			height: 'auto',
			display: 'inline-block',
		},
	},
	PlaceParentQuarterContainer: {
		[CoreStyle.PC.BREAKPOINT]:{
			marginLeft: '-5px',
			marginRight: '-5px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginLeft: '-8px',
			marginRight: '-8px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginLeft: '-12px',
			marginRight: '-12px',
			paddingTop: '3%',
		},
	},
	PlaceParentSubQuarterContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
			display: 'inline-flex',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			whiteSpace: 'nowrap',
			overflowX: 'auto',
			overflowY: 'hidden',
			transition: '-ms-transform 0.5s, -webkit-transform 0.5s, transform 0.5s',
			padding: '0px 18px',
		}
	},
	FifthContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '20%',
			height: 'auto',
			display: 'inline-block',
			padding: '0.5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '50%',
			height: 'auto',
			display: 'inline-block',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '40%',
			height: 'auto',
			display: 'inline-block',
		},
	}
});

export default class Items extends React.Component {

	render() {                    
		let index=0;
		const eachElement = this.props.data.map(function(data) {
			switch(data.type.toUpperCase()) {
				case Constants.Type.Place:
					return(
							<div key={index++} className={css(Style.FifthContainer)}>  
								<Place
									data={data}
								/>
					   		</div>
				   		);
					break;
				case Constants.Type.Journey:
					return(
							<div key={index++} className={css(Style.QuarterContainer)}>  
								<Journey
									data={data}
								/>
					   		</div>
				   		);
					break;
				case Constants.Type.Activity:
					return(
							<div key={index++} className={css(Style.QuarterContainer)}>  
								<Activity
									data={data}
								/>
					   		</div>
				   		);
					break;
				case Constants.Type.Excursion:
					return(
							<div key={index++} className={css(Style.ThirdContainer)}>  
								<Excursion
									data={data}
								/>
					   		</div>
				   		);
					break;
				case Constants.Type.Event:
					return(
							<div key={index++} className={css(Style.ThirdContainer)}>  
								<Event
									data={data}
								/>
					   		</div>
				   		);
					break;
				default:
					return(<div/>);
			}
		});
		switch(this.props.type.toUpperCase()) {
			case Constants.Type.Place:
				return(
						<div className={css(Style.Parent)}>
							<div className={css(Style.PlaceParentQuarterContainer)}>
								<div className={css(Style.PlaceParentSubQuarterContainer)}>
									{eachElement}
								</div>
							</div>
						</div>
			   		);
				break;
			case Constants.Type.Journey:
				return(
						<div className={css(Style.Parent)}>
							<div className={css(Style.ParentQuarterContainer)}>
								<div className={css(Style.ParentSubQuarterContainer)}>
									{eachElement}
								</div>
							</div>
						</div>
			   		);
				break;
			case Constants.Type.Activity:
				return(
						<div className={css(Style.Parent)}>
							<div className={css(Style.ParentQuarterContainer)}>
								<div className={css(Style.ParentSubQuarterContainer)}>
									{eachElement}
								</div>
							</div>
						</div>
			   		);
				break;
			case Constants.Type.Excursion:
				return(
						<div className={css(Style.Parent)}>
							<div className={css(Style.ParentQuarterContainer)}>
								<div className={css(Style.ParentSubQuarterContainer)}>
									{eachElement}
								</div>
							</div>
						</div>
			   		);
				break;
			case Constants.Type.Event:
				return(
						<div className={css(Style.Parent)}>
							<div className={css(Style.ParentQuarterContainer)}>
								<div className={css(Style.ParentSubQuarterContainer)}>
									{eachElement}
								</div>
							</div>
						</div>
			   		);
				break;
			default:
				return(<div/>);
		}
	}
}

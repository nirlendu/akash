/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from 'app/config/core/style'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			marginTop: '3%',
			marginLeft: '0.5%',
			marginRight: '0.5%',
			marginBottom: '1%',
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginTop: '15%',
			marginLeft: '5%',
			marginRight: '5%',
			marginBottom: '1%',
			textAlign: 'center',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginTop: '7%',
			marginLeft: '5%',
			marginRight: '5%',
			marginBottom: '7%',
			textAlign: 'center',
		},
	},
	Intro : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '25px',
			color: [CoreStyle.COLOR.BLACK],
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginBottom: '2%',
			fontWeight: 'bold',
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginBottom: '2%',
			fontWeight: 'bold',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
		},
	},
	FlashStyle:{
		fill: 'rgba(240, 0, 128, 0.57)',
	},
	Title : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontWeight: 'bold',
			fontSize: '50px',
			padding: '1% 0px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontWeight: 'bold',
			fontSize: '35px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontWeight: 'bold',
			fontSize: '32px',
		},
	},
	FirstWord:{
		color : [CoreStyle.COLOR.YELLOW],
	},
	SecondWord:{
		color : [CoreStyle.COLOR.RED],
	},
	Description : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontWeight: 'normal',
			fontSize: '25px',
			color: [CoreStyle.COLOR.GREY],
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			marginTop: '2%',
			fontWeight: 'normal',
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			 color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			marginTop: '2%',
			fontWeight: 'normal',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			 color: [CoreStyle.COLOR.GREY],
		},
	}
})

class TopMessage extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.Intro)}>
					Reliable & Efficient way to attain & retain customers
				</div>
				<div className={css(Style.Title)}>
					<span className={css(Style.FirstWord)}>Introducing &nbsp;</span> 
					<span className={css(Style.SecondWord)}>Panally</span>
				</div>
				<div className={css(Style.Description)}>
					Affordable & Mass outreach made easy
				</div>
			</div>
		);
	}
}

export default TopMessage;

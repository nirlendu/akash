/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import Url from  'app/config/core/url'
import CoreStyle from 'app/config/core/style'

const Style = StyleSheet.create({
	Parent:{
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			marginTop: '3%',
			padding: '3% 35%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			padding: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			margin: '2%',
			width: '100%',
			paddingTop: '15%',
			paddingBottom: '10%',
			paddingLeft: '2%',
		},
	},
	Container:{
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'inline-block',
			height: '100%',
			verticalAlign: 'top',
			padding: '0% 5%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'inline-block',
			height: '100%',
			verticalAlign: 'top',
			padding: '0% 5%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'inline-block',
			height: '100%',
			verticalAlign: 'top',
			padding: '0% 3%'
		},
	},
	ContainerImage:{
		[CoreStyle.PC.BREAKPOINT]:{
			height: '100%',
			width: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			height: '100%',
			width: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			height: '100%',
			width: 'auto',
		},
	}
})

export default class Partners extends React.Component {
	render() {
		const MsgImage = Url.Static.App.Endpoint + 'partners/msg91.png';
		const NasscomImage = Url.Static.App.Endpoint + 'partners/10000-Startups.png';
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.Container)}>
					<a href="https://msg91.com/startups/?utm_source=startup-banner">
					<img src={MsgImage} className={css(Style.Image)} title="MSG91 - SMS for Startups" alt="Bulk SMS - MSG91"/>
					</a>
				</div>
				<div className={css(Style.Container)}>
					<a href="http://10000startups.com/">
					<img src={NasscomImage} className={css(Style.Image)} alt="Nasscom"/>
					</a>
				</div>
			</div>
		);
	}
}

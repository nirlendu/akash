/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'
import Share from 'app/common/containers/share/Share'

const Style = StyleSheet.create({
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '2%'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '2%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '10%'
		},
	},
	Title : {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '1%',
			textAlign: 'center',
			fontSize: '30px',
			fontWeight: 'bold'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '2%',
			textAlign: 'center',
			fontSize: '30px',
			fontWeight: 'bold'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '2%',
			textAlign: 'center',
			fontSize: '25px',
			fontWeight: 'bold'
		},
	},
	BusinessName : {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '2%',
			textAlign: 'center',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.RED]
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '4%',
			textAlign: 'center',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.RED]
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '4%',
			textAlign: 'center',
			fontSize: '20px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.RED]
		},
	},
	TermsBox: {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '1% 25%',
			textAlign: 'left',
			fontSize: '20px',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '1% 20%',
			textAlign: 'left',
			fontSize: '20px',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '1% 5%',
			textAlign: 'left',
			fontSize: '17px',
		},
	},
	TermsTitle: {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingBottom: '1%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLUE]
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingBottom: '1%',
			fontSize: '25px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLUE]
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingBottom: '1%',
			fontSize: '20px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLUE]
		},
	},
});

export default class CampaignBody extends React.Component {
	render() {
		let index = 0;
		const eachTerm = this.props.data.terms.map(function(term) {
			return(
				<div key={index++}>
					<span>* </span>{term}
				</div>
			)
		});
		const Title = 'Refer a friend & both gets ' + this.props.data.campaignDetails;
		return (
			<div>
				<div className={css(Style.Title)}>
					{Title}
				</div>
				<div className={css(Style.BusinessName)}>
					<span>@ </span>{this.props.data.businessName}
				</div>
				<Share
					businessName={this.props.data.businessName}
					campaignId={this.props.data.campaignId}
					campaignDetails={this.props.data.campaignDetails}
				/>
				<div className={css(Style.TermsBox)}>
					<div className={css(Style.TermsTitle)}>
						Terms & Conditions
					</div>
					{eachTerm}
				</div>
			</div>
		)
	}
}

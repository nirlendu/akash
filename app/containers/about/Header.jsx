/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '30px',
			color: [CoreStyle.COLOR.BLACK],
			textAlign: 'center',
			fontWeight: 'bold',
			padding: '3% 0%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: '20px',
			color: [CoreStyle.COLOR.BLACK],
			textAlign: 'center',
			fontWeight: 'bold',
			padding: '3% 0%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: '20px',
			color: [CoreStyle.COLOR.BLACK],
			textAlign: 'center',
			fontWeight: 'bold',
			padding: '10% 0%',
		},
	},
})

export default class Header extends React.Component {
	render() {
		return (
			<div className={css(Style.Wrapper)}>
				{this.props.data}
			</div>
		)
	}
}

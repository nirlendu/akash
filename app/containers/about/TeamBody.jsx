/**
 * Copyright - Panally Internet
 */

 /*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import ProgressiveImage from 'react-progressive-image'

import CoreStyle from 'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '0% 15%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
		},
	},
	Block: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '50%',
			paddingLeft: '5%',
			paddingRight: '5%',
			paddingBottom: '5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '50%',
			paddingLeft: '5%',
			paddingRight: '5%',
			paddingBottom: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			padding: '5%',
		},
	},
	ImageContainer: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			padding: '0% 20%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '50%',
			padding: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			padding: '0% 25%',
		},
	},
	Image: {
		borderRadius: '50%',
		width: '100%',
	},
	AboutTitle: {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '5% 0%',
			fontSize: '20px',
			color: [CoreStyle.COLOR.BLACK],
			fontWeight: 'bold',
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '5% 0%',
			fontSize: '17px',
			color: [CoreStyle.COLOR.BLACK],
			fontWeight: 'bold',
			textAlign: 'center',
			padding: '10%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '5% 0%',
			fontSize: '17px',
			color: [CoreStyle.COLOR.BLACK],
			fontWeight: 'bold',
			textAlign: 'center',
			padding: '5% 0%',
		},
	},
	AboutDescription: {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '20px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: '17px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			padding: '10%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: '17px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			padding: '0% 10%',
		},
	},
	LinkedInButton: {
		width: '100%',
		padding: '5% 45%',
	}
})

export default class TeamBody extends React.Component {
	render() {
		const GauravImage = Url.Static.App.Endpoint + 'team/gaurav.jpg';
		const NirlenduImage = Url.Static.App.Endpoint + 'team/nirlendu.jpg';
		const LinkedInIcon = Url.Static.App.Endpoint + 'team/linkedin.png';
		return (
			<div className={css(Style.Wrapper)}>
				<div className={css(Style.Block)}>
					<div className={css(Style.ImageContainer)}>
						<img className={css(Style.Image)} src={GauravImage} />
					</div>
					<div className={css(Style.AboutTitle)}>
						Gaurav Jain, Business & Operations
					</div>
					<div className={css(Style.AboutDescription)}>
						CS 2015, NITK Surathkal<br/>He worked with Flipkart Bangalore before starting Panally. Starting his first venture at the age of 19, Gaurav and his business abilities goes a long way back.
					</div>
					<div className={css(Style.LinkedInButton)}>
						<a href='https://www.linkedin.com/in/gauravjain7' target="_blank" rel="noopener noreferer">
							<img className={css(Style.Image)} src={LinkedInIcon} />
						</a>
					</div>
				</div>
				<div className={css(Style.Block)}>
					<div className={css(Style.ImageContainer)}>
						<img className={css(Style.Image)} src={NirlenduImage} />
					</div>
					<div className={css(Style.AboutTitle)}>
						Nirlendu Saha, Product & Tech
					</div>
					<div className={css(Style.AboutDescription)}>
						CS 2015, NITK Surathkal<br/> He found himself in Rakuten, Tokyo right after college. Nirlendu likes to build things, and has keen eyes on product design.
					</div>
					<div className={css(Style.LinkedInButton)}>
						<a href='https://www.linkedin.com/in/nirlendu' target="_blank" rel="noopener noreferer">
							<img className={css(Style.Image)} src={LinkedInIcon} />
						</a>
					</div>
				</div>
			</div>
		)
	}
}

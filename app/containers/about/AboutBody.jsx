/**
 * Copyright - Panally Internet
 */

 /*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'
import ProgressiveImage from 'react-progressive-image'

import CoreStyle from 'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Wrapper : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '20px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: '17px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			padding: '10%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: '17px',
			color: [CoreStyle.COLOR.GREY],
			textAlign: 'center',
			padding: '0% 10%',
		},
	},
})

export default class AboutBody extends React.Component {
	render() {
		return (
			<div className={css(Style.Wrapper)}>
				Panally is an initiative for enabling businesses to reach out their audience seamlessly.<br/><br/>Through our platform, businesses can create a mass out reach channel with a minimum marketing budget.<br/><br/><br/> 
			</div>
		)
	}
}

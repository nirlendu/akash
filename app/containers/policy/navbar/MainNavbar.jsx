/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			backgroundColor: 'rgba(0,0,0,0.07)',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			backgroundColor: 'rgba(0,0,0,0.07)',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			backgroundColor: 'rgba(0,0,0,0.07)',
		},
	},
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '50%',
			marginLeft: '25%',
			marginRight: '25%',
			paddingTop: '3%',
			paddingBottom: '2%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			paddingTop: '3%',
			paddingBottom: '2%'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			paddingTop: '10%',
			paddingBottom: '5%'
		},
	},
	PolicyLinks: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
			textAlign: 'center',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
			textAlign: 'center',
		},
		color: [CoreStyle.COLOR.BLACK],
		':hover': {
			cursor: 'pointer',
			color: [CoreStyle.COLOR.RED],
		},
	},
})

export default class Policy extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.Container)}>
					<a key='1' href='/policy/privacy' className={css(Style.PolicyLinks)}>
						Privacy
					</a>
					<a key='2' href='/policy/terms' className={css(Style.PolicyLinks)}>
						T's & C's
					</a>
				</div>
			</div>
		);
	}
}

/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			paddingTop: '4%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			paddingTop: '10%',
			paddingLeft: '5%',
			paddingRight: '5%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			paddingTop: '10%',
			paddingLeft: '5%',
			paddingRight: '5%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
	},
})

export default class Policy extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
			<p>
			By using the Website, you confirm that you accept these terms and that you agree to comply with them. If you do not agree to these terms, you must not use the website.<br/><br/>
			We may revise these terms at any time by amending this page. We may, without prior notice, change the services; add or remove functionalities or features; stop providing the services or features of the services, to you or to users generally; or create usage limits for the services.<br/><br/>
			We may update the website from time to time and may change the content at any time. However, please note that any of the content on the website may be out of date at any given time and we are under no obligation to update it.
			We do not guarantee that the Website, or any content on it, will be free from errors or omissions.<br/><br/>
			We do not guarantee that your use of the website, or any content on it, will always be available or be uninterrupted. We may suspend, withdraw, discontinue or change all or any part of the Website without notice. We will not be liable to you if for any reason the Website is unavailable at any time or for any period.<br/><br/>
			</p>
			</div>
		);
	}
}

/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from  'app/config/core/style'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			paddingTop: '4%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			paddingTop: '10%',
			paddingLeft: '5%',
			paddingRight: '5%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			paddingTop: '10%',
			paddingLeft: '5%',
			paddingRight: '5%',
			color: [CoreStyle.COLOR.GREY],
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			textAlign: 'left',
		},
	},
})


export default class Privacy extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
				<span className={css(Style.Highlight)}><b>Collection of Personal Information</b></span>

				<br/>
				 Panally collects personal information provided by you when you make a booking and/or when you contact us for any help. The personal information we collect may include the following: Name, email address, your home and/or mobile phone numbers, your gender, and your date of birth. We may collect additional personal information in connection with your participation in any promotions offered by us.<br/>
				<br/>

				<span className={css(Style.Highlight)}><b>Use of Information</b></span>

				<br/>
				We may use personal information collected from you for several general purposes:<ul>
				
				<li>To deliver the products and services that you have requested</li>
				<li>To respond to your inquiries</li>
				<li>To manage your account and provide you with customer support including for administrative or operational purposes relating to our service</li>
				<li>To send you offers and news about Panally</li>
				<li>To conduct market research relating to the development of our products, services, and/or Website</li>
				<li>To develop and display content and advertising tailored to your interests on our Website and other sites</li>
				<li>To verify your eligibility and deliver prizes in connection with contests and sweepstakes</li>
				<li>To otherwise maintain and administer the Website</li>
				<li>To perform functions as otherwise described to you at the time of collection</li>
				</ul>
				<br/>

				<span className={css(Style.Highlight)}><b>Use of cookie and IP address </b></span>
				
				<br/> 
				This website uses cookies to better the users experience while visiting the website. Furthermore, to better tailor our website and product offerings to our customers, we use Google Analytics to collect information on how visitors navigate our website. Cookies are used to collect aggregated data that allows us to see the origins of web traffic, amount of visitors, visitors’ operating systems, browser types, etc. This data does not personally identify you in any way. You can choose not to accept cookies by disabling them in the settings of your web browser. This complies with recent legislation requirements for websites to obtain explicit consent from users before leaving behind or reading files such as cookies on a user's computer / device.
				<br/><br/>

				<span className={css(Style.Highlight)}><b>Third party links and images</b></span>
				
				<br/> 
				Our Website may contain links to other websites which are outside our control and are not covered by this Privacy Policy (“Third-Party Websites”). If you access Third-Party Websites using the links provided, the operators of such sites may collect information from you which will be used by them in accordance with their own privacy policies, which may differ from ours. We encourage you to review the privacy policies of those Third-Party Websites so that you understand if/how they collect and/or use information from you or your computer. <br/><br/>

				<span className={css(Style.Highlight)}><b> <i>All rights reserved: No content on our website may be used or reproduced for any commercial purposes. Panally reserves the right to amend this privacy policy at any time.</i></b><br/></span>
			</div>
		);
	}
}

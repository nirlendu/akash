/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react';
import { StyleSheet, css } from 'aphrodite'

import Url from 'app/config/core/url'
import CoreStyle from 'app/config/core/style'

const Style = StyleSheet.create({
	Parent : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			backgroundColor: [CoreStyle.COLOR.BLACK],
			color: [CoreStyle.COLOR.WHITE],
			zIndex: '-999',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			backgroundColor: [CoreStyle.COLOR.BLACK],
			color: [CoreStyle.COLOR.WHITE],
			zIndex: '-999',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			backgroundColor: [CoreStyle.COLOR.BLACK],
			color: [CoreStyle.COLOR.WHITE],
			zIndex: '-999',
		},
	},
	Block : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			position : 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			paddingTop: '4%',
			paddingBottom: '4%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			position : 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			paddingTop: '5%',
			paddingBottom: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			position : 'relative',
			marginLeft: 'auto',
			marginRight: 'auto',
			paddingTop: '5%',
			paddingBottom: '5%',
		},
	},
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'block',
			width: '33%',
			verticalAlign : 'top',
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			verticalAlign : 'top',
			textAlign: 'center',
			paddingTop: '5%',
			paddingBottom: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
			verticalAlign : 'top',
			textAlign: 'center',
			paddingTop: '5%',
			paddingBottom: '5%',
		},
	},
	LinkTitle: {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
	},
	LinkContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingTop: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingTop: '5%',
		},
	},
	Links:{
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			textAlign: 'center',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			textAlign: 'center',
			fontSize: [CoreStyle.TAB.SMALL_FONTSIZE],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			textAlign: 'center',
			fontSize: [CoreStyle.MOB.SMALL_FONTSIZE],
		},
		color: 'grey',
		':hover': {
			cursor: 'pointer',
			color: 'white',
		},
	},
	ConnectTitle: {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: [CoreStyle.PC.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
		},
	},
	ConnectContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingTop: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingTop: '5%',
		},
	},
	Connect:{
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
		   fontSize: [CoreStyle.TAB.SMALL_FONTSIZE],
		   color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
		   fontSize: [CoreStyle.MOB.SMALL_FONTSIZE],
		   color: [CoreStyle.COLOR.GREY],
		},
	},
	ImageBlock : {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			paddingTop: '5%',
			width: '50%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			paddingTop: '5%',
			width: '50%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			paddingTop: '10%',
			width: '50%',
		},
	},
	SocialMediaImageContainer : {
		[CoreStyle.PC.BREAKPOINT]:{
			verticalAlign : 'top',
			width: '33%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			verticalAlign : 'top',
			width: '33%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			verticalAlign : 'top',
			width: '33%',
		},
		':hover': {
			cursor: 'pointer',
		},
	},
	SocialMediaImage : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '30%',
			marginLeft: '35%',
			marginRight: '35%',
			height: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '30%',
			marginLeft: '35%',
			marginRight: '35%',
			height: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '30%',
			marginLeft: '35%',
			marginRight: '35%',
			height: 'auto',
		},
	},
	ImageContainer: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '40%',
			marginLeft: '30%',
			marginRight : '30%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '20%',
			marginLeft: '40%',
			marginRight : '40%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '40%',
			marginLeft: '30%',
			marginRight : '30%',
		},
	},
	Image: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			height: 'auto',
		},
	},
	LogoText : {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '10%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingTop: '10%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingTop: '10%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
		},
	},
	Heart : {
		fontSize: '100%',
		color: 'red',
	},
	One: {
		'backgroundPosition': '-10px -10px',
		'width':'32px', 
		'height':'32px',
	},
	Two: {
		'backgroundPosition': '-62px -10px',
		'width':'32px', 
		'height':'32px',
	},
	Three: {
		'backgroundPosition': '-10px -62px',
		'width':'32px', 
		'height':'32px',
	},
	SpriteParent: {
		backgroundImage: 'url(' + [Url.Static.App.Endpoint] + 'footer/sprite.png)',
		margin: 'auto',
	},
	ManualPayment: {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '5%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingTop: '5%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingTop: '10%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
	},
	About : {
		[CoreStyle.PC.BREAKPOINT]:{
			paddingTop: '2%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			paddingTop: '2%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			paddingTop: '4%',
			width: '100%',
			fontSize: [CoreStyle.PC.SMALL_FONTSIZE],
		},
	},
	PolicyLinks : {
		color: [CoreStyle.COLOR.BLUE],
		':hover': {
			color: [CoreStyle.COLOR.YELLOW],
			cursor: 'pointer',
		},
	},
	AboutText : {
		color: [CoreStyle.COLOR.WHITE],
		':hover': {
			color: [CoreStyle.COLOR.YELLOW],
			cursor: 'pointer',
		},
	}
})


class Links extends React.Component {
	render() {
		return (
			<div className={css(Style.Container)}>
				<div className={css(Style.LinkTitle)}>
					Terms and Policies
				</div>
				<div className={css(Style.LinkContainer)}>
					<div key='1' className={css(Style.Links)}>
						<a href='/policy/privacy' target="_blank" rel="noopener noreferer" className={css(Style.PolicyLinks)}>
							Privacy Policy
						</a>
					</div>
					<div key='2' className={css(Style.Links)}>
						<a href='/policy/terms' target="_blank" rel="noopener noreferer" className={css(Style.PolicyLinks)}>
							Terms and Conditions
						</a>
					</div>
				</div>
				<div className={css(Style.About)}>
					<a href='/about' className={css(Style.AboutText)}>
						About Us
					</a>
				</div>
			</div>
		);
	}
}


class Connect extends React.Component {
	render() {
		var FacebookURL = 'https://www.facebook.com/panallydotcom';
		var TwitterURL = 'https://twitter.com/panallydotcom';
		var InstagramURL = 'https://www.instagram.com/panallydotcom';
		return (
			<div className={css(Style.Container)}>
				<div className={css(Style.ConnectTitle)}>
					Please do get in touch
				</div>
				<div className={css(Style.ConnectContainer)}>
					<div key='1' className={css(Style.Connect)}>
						team@panally.com
					</div>
					<div key='2' className={css(Style.Connect)}>
						+91-8277405700
					</div>
					<div key='3' className={css(Style.Connect)}>
						+91-9990426229
					</div>
				</div>
				<div className={css(Style.ImageBlock)}>
					<a key='1' href={FacebookURL} target="_blank" rel="noopener noreferer" className={css(Style.SocialMediaImageContainer)} >
						<div className={css([
								Style.One, 
								Style.SpriteParent
							])}
						/>
					</a><br/>
					<a key='2' href={InstagramURL} target="_blank" rel="noopener noreferer" className={css(Style.SocialMediaImageContainer)} >
						<div className={css([
								Style.Two, 
								Style.SpriteParent
							])}
						/>
					</a><br/>
					<a key='3' href={TwitterURL} target="_blank" rel="noopener noreferer" className={css(Style.SocialMediaImageContainer)} >
						<div className={css([
								Style.Three, 
								Style.SpriteParent
							])}
						/>
					</a>
				</div>
			</div>
		);
	}
}


class Logo extends React.Component {
	render() {
		var FooterImage = Url.Static.App.Endpoint + 'footer/panally.jpg';
		return (
			<div className={css(Style.Container)}>
				<div className={css(Style.ImageContainer)} >
					<img src={FooterImage} className={css(Style.Image)} alt="panally black logo"/>
				</div>
				<div className={css(Style.LogoText)}>
					&#169; 2017 Panally Internet
					<br/>All Rights Reserved
				</div>
				<div className={css(Style.LogoText)}>
					Designed and crafted with &nbsp;
					<span className={css(Style.Heart)}>&hearts;</span>
						&nbsp; in India
				</div>
			</div>
		);
	}
}


class Footer extends React.Component {
	render() {
		return (
			<div className={css(Style.Parent)}>
				<div className={css(Style.Block)}>
					<Links/>
					<Connect/>
					<Logo/>
				</div>
			</div>
		);
	}
}

export default Footer;

/**
 * Copyright - Panally Internet
 */

/*
 global require module
 */

import React from 'react'
import { StyleSheet, css } from 'aphrodite'

import CoreStyle from 'app/config/core/style'
import Url from 'app/config/core/url'

const Style = StyleSheet.create({
	Container : {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			padding: '3% 0px',
			textAlign: 'center',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			padding: '5% 0px',
			textAlign: 'center',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			padding: '5% 0px',
			textAlign: 'center',
		},
	},
	Header : {
		[CoreStyle.PC.BREAKPOINT]:{
			fontSize: '25px',
			textAlign : 'center',
			color : [CoreStyle.COLOR.BLACK],
			fontWeight : 'bold',
			paddingBottom: '2%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: [CoreStyle.TAB.TITLE_FONTSIZE],
			textAlign : 'center',
			color : [CoreStyle.COLOR.BLACK],
			fontWeight : 'bold',
			paddingBottom: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: [CoreStyle.MOB.TITLE_FONTSIZE],
			textAlign : 'center',
			color : [CoreStyle.COLOR.BLACK],
			fontWeight : 'bold',
			paddingBottom: '5%',
		},
	},
	Body: {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '0px 1%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '100%',
		},
	},
	SubBody: {
		[CoreStyle.PC.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '50%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '0% 1%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: '-webkit-inline-flex',
			display: '-ms-inline-flexbox',
			display: 'inline-flex',
			width: '100%',
			padding: '0% 1%',
		},
	},
	SubContainerParent: {
		[CoreStyle.PC.BREAKPOINT]:{
			display: 'block',
			width: '46%',
			margin: '2%',
			border: '2px solid rgba(0,0,0,0.07)'
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			display: 'block',
			width: '48%',
			margin: '1%',
			border: '2px solid rgba(0,0,0,0.07)'
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			display: 'block',
			width: '48%',
			margin: '1%',
			border: '2px solid rgba(0,0,0,0.07)'
		},
	},
	SubContainer: {
		[CoreStyle.PC.BREAKPOINT]:{
			padding: '10% 5%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			padding: '5%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			padding: '10% 5%',
		},
	},
	Title: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			fontSize: '20px',
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLACK],
			paddingTop: '10%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			width: '100%',
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLACK],
			paddingTop: '10%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			width: '100%',
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			fontWeight: 'bold',
			color: [CoreStyle.COLOR.BLACK],
			paddingTop: '10%',
		},
	},
	Description: {
		[CoreStyle.PC.BREAKPOINT]:{
			width: '100%',
			fontSize: '17px',
			color: [CoreStyle.COLOR.GREY],
			padding: '1% 3%',
		},
		[CoreStyle.TAB.BREAKPOINT]:{
			fontSize: [CoreStyle.TAB.DESCRIPTION_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
			padding: '3%',
		},
		[CoreStyle.MOB.BREAKPOINT]:{
			fontSize: [CoreStyle.MOB.DESCRIPTION_FONTSIZE],
			color: [CoreStyle.COLOR.GREY],
			paddingTop: '7%',
		},
	},
	Pricing: {
		'backgroundPosition': '-94px -94px',
		'width':'64px', 
		'height':'64px',
	},
	Profits: {
		'backgroundPosition': '-94px -10px',
		'width':'64px', 
		'height':'64px',
	},
	Retention: {
		'backgroundPosition': '-10px -94px',
		'width':'64px', 
		'height':'64px',
	},
	Trust: {
		'backgroundPosition': '-10px -10px',
		'width':'64px', 
		'height':'64px',
	},
	SpriteParent: {
		backgroundImage: 'url(' + [Url.Static.App.Endpoint] + 'about/sprite.png)',
		margin: 'auto',
	},
});


class About extends React.Component {
		
	render() {
		return (
			<div id="about" className={css(Style.Container)}>
				<div className={css(Style.Header)}>
					Why Panally?
				</div>
				<div className={css(Style.Body)}>
					<div className={css(Style.SubBody)}>
						<div className={css(Style.SubContainerParent)}>  
							<div className={css(Style.SubContainer)}>                              
								<div className={css([
										Style.Pricing, 
										Style.SpriteParent
									])}
								/>
								<div className={css(Style.Title)}>
									Spend Less
								</div>
								<div className={css(Style.Description)}>
									Simple and fair pricing - Pay per volume
								</div>
							</div>
						</div>
						<div className={css(Style.SubContainerParent)}>  
							<div className={css(Style.SubContainer)}>
								<div className={css([
										Style.Profits, 
										Style.SpriteParent
									])}
								/>
								<div className={css(Style.Title)}>
									More Profits
								</div>
								<div className={css(Style.Description)}>
									Spending less on customer acquisition results in better profits
								</div>   
							</div>
						</div>
					</div>
					<div className={css(Style.SubBody)}>
						<div className={css(Style.SubContainerParent)}> 
							<div className={css(Style.SubContainer)}>
								<div className={css([
										Style.Retention, 
										Style.SpriteParent
									])}
								/>
								<div className={css(Style.Title)}>
									Increased Retention
								</div>
								<div className={css(Style.Description)}>
									Increase customer retention rate with our approach
								</div> 
							</div>
						</div>
						<div className={css(Style.SubContainerParent)}> 
							<div className={css(Style.SubContainer)}>
								<div className={css([
										Style.Trust, 
										Style.SpriteParent
									])}
								/>
								<div className={css(Style.Title)}>
									Efficient approach
								</div>
								<div className={css(Style.Description)}>
									Including trust factor for better conversions
								</div> 
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

module.exports = About;
